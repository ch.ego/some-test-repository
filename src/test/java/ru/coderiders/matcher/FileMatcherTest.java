package ru.coderiders.matcher;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

abstract class FileMatcherTest<T extends FileMatcher> {
    T matcher;

    @BeforeEach
    void setUp() {
        matcher = init();
    }

    abstract T init();

    @Test
    public void fileMustNotExist() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> matcher.match(file));
    }

    @Test
    void fileIsADirectory() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(true);
        assertFalse(matcher.match(file));
    }

    @Test
    void matchCorrect() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        String extension = matcher.getExtensions().iterator().next();
        when(file.getName()).thenReturn(extension);
        assertTrue(matcher.match(file));
        assertDoesNotThrow(() -> matcher.match(file));
    }

}