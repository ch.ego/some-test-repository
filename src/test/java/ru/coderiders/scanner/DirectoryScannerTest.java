package ru.coderiders.scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.coderiders.matcher.FileMatcher;
import ru.coderiders.matcher.PictureMatcher;

import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DirectoryScannerTest {
    Path path;
    FileMatcher matcher;
    DirectoryScanner directoryScanner;

    @BeforeEach
    void init() {
        path = mock(Path.class);
        matcher = new PictureMatcher();
        directoryScanner = new DirectoryScanner(path, matcher, true);
    }

    @Test
    public void something() {
        when(path.toFile().exists()).thenReturn(false);
        assertThrows(NullPointerException.class, () -> directoryScanner.scan());
    }


}