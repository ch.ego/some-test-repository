package ru.coderiders.matcher;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PdfMatcherTest extends FileMatcherTest<PdfMatcher> {
    PdfMatcher pdfMatcher = new PdfMatcher();

    @Override
    PdfMatcher init() {
        return pdfMatcher;
    }

    @Test
    void getExtensions() {
        assertNotNull(pdfMatcher.getExtensions());
        assertDoesNotThrow(() -> pdfMatcher.getExtensions());
    }


}