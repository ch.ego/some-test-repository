package ru.coderiders.matcher;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PictureMatcherTest extends FileMatcherTest<PictureMatcher> {
    PictureMatcher pictureMatcher = new PictureMatcher();

    @Override
    PictureMatcher init() {
        return pictureMatcher;
    }

    @Test
    void getExtensions() {
        assertNotNull(pictureMatcher.getExtensions());
        assertDoesNotThrow(() -> pictureMatcher.getExtensions());
    }



}